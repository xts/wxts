--
-- Database: `xaseco`
--

-- --------------------------------------------------------

--
-- Table structure for table `xts`
--

CREATE TABLE IF NOT EXISTS `xts` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `PlayerId` mediumint(9) unsigned NOT NULL COMMENT 'Player-ID',
  `ChallengeId` mediumint(9) unsigned NOT NULL COMMENT 'Challenge-ID',
  `Times` varchar(350) NOT NULL COMMENT 'Times, comma speparated',
  `Count` int(6) unsigned NOT NULL COMMENT 'overall count of attemped runs',
  `Mode` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Race-mode',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='(X)Aseco Training Statistics' AUTO_INCREMENT=0 ;
