<?php
namespace Wxts;
/**
 * wXTS - A web frontend for the xts XAseco plugin.
 *
 * License: WTFPL - http://sam.zoy.org/wtfpl/
 *
 * Check the doc directory for a changelog, installation information etc.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */

// Load core application config
include_once('application/config/application.php');

try {

  // Process the HTTP request using only the routers we need for this application.
  $fc = new FrontController();
  $fc->addRouter(new \Lvc_RegexRewriteRouter($regexRoutes));
  $fc->processRequest(new \Lvc_HttpRequest());

} catch (\Lvc_Exception $e) {

  // Log the error message
  var_dump($e->getMessage());

  // Get a request for the 404 error page.
  $request = new \Lvc_Request();
  $request->setControllerName('error');
  $request->setActionName('view');
  $request->setActionParams(array('error' => '404'));

  // Get a new front controller without any routers, and have it process our handmade request.
  $fc = new FrontController();
  $fc->processRequest($request);

}