<?php

// Derived Constants
// (NOTE that these are not needed by LightVC, but are usually useful in the app layer.)
define('APP_PATH', dirname(dirname(__FILE__)) . '/');
define('WWW_BASE_PATH', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('WWW_CSS_PATH', WWW_BASE_PATH . 'css/');
define('WWW_JS_PATH', WWW_BASE_PATH . 'js/');
define('WWW_IMAGE_PATH', WWW_BASE_PATH . 'images/');
// Change this before releasing
define('ENVIRONMENT', 'development');
//define('ENVIRONMENT', 'production');

// Include and configure the LighVC framework
include_once(APP_PATH . 'modules/lightvc/lightvc.php');
Lvc_Config::addControllerPath(APP_PATH . 'controllers/');
Lvc_Config::addControllerViewPath(APP_PATH . 'views/');
Lvc_Config::addLayoutViewPath(APP_PATH . 'views/layouts/');
Lvc_Config::addElementViewPath(APP_PATH . 'views/elements/');
Lvc_Config::setViewClassName('Wxts\\View');

// Load Routes
include(dirname(__FILE__) . '/routes.php');

// wXTS configuration

// Time zone
date_default_timezone_set('Europe/Berlin');

// Translation path
//define('TRANSLATION_PATH', APP_PATH . 'lang/');

// Autoloader config
define('CLASS_PATH', APP_PATH . 'library/');

// Add class dir to include path
set_include_path(get_include_path().PATH_SEPARATOR.CLASS_PATH);

// Include color parser
require 'tmfcolorparser/classes/tmfcolorparser.inc.php';

// Autoloader
require 'Wxts/AutoLoader.php';
Wxts\AutoLoader::registerAutoloader();