<?php

// Format of regex => parseInfo
$regexRoutes = array(

  '#^(.*)$#' => array(
    'controller' => isset($_GET['c']) ? $_GET['c'] : 'stats',
    'action'	=> isset($_GET['a']) ? $_GET['a'] : 'show',
    'additional_params' => 1,
  ),
);

?>