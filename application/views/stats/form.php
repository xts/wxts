<form method="get">
  Choose server:<br />
  <br />
  <select name="server">
    <?php foreach ($servers as $server): ?>
    <option value="<?php echo $server->getId(); ?>">
      <?php echo $server->getConfig('name'); ?> -
      <?php echo $server->getConfig('description'); ?>
    </option>
    <?php endforeach; ?>
  </select>
  <br />
  Password: <input name="password" type="text" value="" />
  <br />
  <br />
  <br />
  Filter options:<br />
  <br />

  Players:
  <input name="players" type="text" value="" />
  Comma-separated list of logins.<br />
  <br/>
  Maps:
  <input name="maps" type="text" value="" />
  Comma-separated list of IDs (check the table below [which is coming soon])<br />
  <br />
  Modes:
  <select name="mode">
    <option value="-1">All</option>
    <option value="0">Round-based</option>
    <option value="1">Time attack</option>
  </select>

  <br />
  <hr />
  <br />

  Sorting options:<br />
  <br />
  <table>
    <thead><th>Sort by</th><th>Group by</th></thead>
    <br />
    <?php
    $form = $request->toArray();
    for ($i = 0; $i < 3; $i++): ?>
    <tr>
      <td>
        <select name="sort[]">
          <option value=""<?php if (!$form['sort'][$i]) echo ' selected="selected"'; ?>>None</option>
          <option value="player"<?php if ($form['sort'][$i] == 'player') echo ' selected="selected"'; ?>>
            Player
          </option>
          <option value="map"<?php if ($form['sort'][$i] == 'map') echo ' selected="selected"'; ?>>
            Map
          </option>
          <option value="date"<?php if ($form['sort'][$i] == 'date') echo ' selected="selected"'; ?>>
            Date
          </option>
          <option value="mode"<?php if ($form['sort'][$i] == 'mode') echo ' selected="selected"'; ?>>
            Mode
          </option>
          <option value="averageTime"<?php if ($form['sort'][$i] == 'averageTime') echo ' selected="selected"'; ?>>
            Avg. time
          </option>
        </select>
      </td>
      <td>
        <select name="group[]">
          <option value="" <?php if (!$form['group'][$i]) echo 'selected="selected"'; ?>>
            None
          </option>
          <option value="player" <?php if ($form['group'][$i] == 'player') echo 'selected="selected"'; ?>>
            Player
          </option>
          <option value="map" <?php if ($form['group'][$i] == 'map') echo 'selected="selected"'; ?>>
            Map
          </option>
          <option value="date" <?php if ($form['group'][$i] == 'date') echo 'selected="selected"'; ?>>
            Date
          </option>
        </select>
      </td>
    </tr>
    <?php endfor; ?>

  </table>

  <br />
  <br />
  <input name="form" type="submit" value="Submit" />

</form>