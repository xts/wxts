<div id="queryInformation">
<table class="blockTable">
<tr><td>Server: <?php echo Wxts\Server::getActiveServer()->getConfig('name'); ?></td></tr>
<tr><td>Filter options:</td><td>Sorting options:</td></tr>
<tr>
  <td>Players: <?php echo implode(', ', $request->getData('players', array('no filtering'))); ?></td>
  <td>Sorted by: <?php echo implode(', ', $sorter->getSortFields()); ?></td>
</tr>
<tr>
  <td>Maps: <?php echo implode(', ', $request->getData('maps', array('no filtering'))); ?></td>
  <td>Grouped by: <?php echo implode(', ', $groupHash->getHashFields()); ?></td>
</tr>
</table>
</div>
<?php

foreach ($recordSet->getRecords() as $record)
{
  $this->renderElement('block', array('data' => $record, 'hash' => $groupHash));
}