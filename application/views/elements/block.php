<br />
<br />
<div class="block">
  <div class="groupIdentifier"><?php echo implode(' -> ', $hash->getHashFieldIdentifiers($data)); ?></div>
  <?php
  foreach (Wxts\Graph_Manager::getInstance()->getGraphUrls($data) as $graphUrl)
  {
    echo '<br /><img src="'.$graphUrl.'" alt="Graph" /><br />';
  }
  ?>
  <table class="blockTable" rules="all">
  <thead>
  <th>MD</th><th>Player</th><th>Map</th><th>Avg. time</th><th>Runs / Fails (%)</th><th>Date</th>
  </thead>
  <?php foreach ($data->getRecords() as $record): ?>
  <tr>
  <td><?php echo Wxts\Record::$modes[$record->getMode()]; ?></td>
  <td>
    <?php echo $record->getPlayer()->getNickname(); ?>
    (<a href="?<?php echo $_SERVER['QUERY_STRING']; ?>&players=<?php echo $record->getPlayer()->getName(); ?>">
    <?php echo $record->getPlayer()->getName(); ?>
    </a>)
  </td>
  <td><?php echo $record->getMap()->getName(); ?></td>
  <td><?php echo $record->getAverageTime(); ?></td>
  <td><?php echo $record->getCount(); ?> /
  <?php echo $record->getFails(); ?> (<?php echo $record->getFails(true); ?>)</td>
  <td><?php echo date("d.m.y", $record->getDate()); ?></td>
  </tr>
  <?php endforeach; ?>
  </table>
</div>
<br />
<br />