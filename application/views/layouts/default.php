<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
   "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type"
        content="application/xhtml+xml; charset=UTF-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <title><?php echo $pageTitle ?> @ wXTS::Turbo</title>
  <?php

  if (isset($requiredCss)) {
    foreach ($requiredCss as $css => $use) {
      echo '<link rel="stylesheet" href="' . WWW_CSS_PATH . $css . '" type="text/css" media="all" charset="utf-8" />' . "\n";
    }
  }

  if (isset($requiredJs)) {
    foreach ($requiredJs as $js => $use) {
      echo '<script type="text/javascript" language="javascript" charset="utf-8" src="' . WWW_JS_PATH . $js . '"></script>' . "\n";
    }
  }

  ?>
</head>

<body>
<div id="header">
<h1><?php echo $pageTitle ?> @ wXTS::turbo v0.8 </h1>
</div>

<div id='content'>
    <?php echo $layoutContent ?>
</div>

<div id="footer">
<b>Powered by</b> <a href="http://www.lightvc.org/">LightVC</a>
</div>

</body>
</html>