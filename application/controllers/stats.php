<?php

/**
 * Main controller, which provides a form to choose which data to display and
 * an action which displays the data.
 */
class StatsController extends Wxts\Controller
{
  protected function beforeAction()
  {
    parent::beforeAction();

    $this->setVar('request', $this->getHelper('request')->getRequest());
  }

  public function actionForm()
  {
    $this->setVar('form', $this->getHelper('request')->getRequest()->toArray());
    $this->setVar('servers', Wxts\Server::listServers());
  }

  public function actionShow()
  {
    $request = $this->getHelper('request')->getRequest();

    // Check server password
    if (Wxts\Server::getActiveServer()->getConfig('password') !=
      $request->getData('password', null))
    {
      die('This server is password-protected (and you got the wrong password).');
    }

    // Load data
    $recordSet = Wxts\DataManager::getInstance()->
      loadData($this->getHelper('request')->getRequest());

    // Create sequencer
    $sequencer = new Wxts\Sequencer($recordSet);

    $recordSet = $sequencer->getRecordSet($request->getData('sort', array()),
      $request->getData('group', array()));

    // setup graph factory
    Wxts\Graph_Manager::getInstance()->setUpFactory($sequencer->getGroupHash());

    // Pass sorter and hash
    $this->setVar('groupHash', $sequencer->getGroupHash());
    $this->setVar('sorter', $sequencer->getSorter());

    $this->setVar('recordSet', $recordSet);
  }
}