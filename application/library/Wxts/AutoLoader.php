<?php
namespace Wxts;

class AutoLoader
{
  protected static $_classPath = CLASS_PATH;

  public static function registerAutoloader()
  {
    return spl_autoload_register(array(__CLASS__,
      isset($_GET['debug']) && ENVIRONMENT == 'development' ? 'loadClassDebug' : 'loadClass'));
  }

  public static function loadClass($className)
  {
    $file = self::$_classPath.
      str_replace(array('_', '\\'), DIRECTORY_SEPARATOR, $className).'.php';

    @include $file;
  }

  public static function loadClassDebug($className)
  {
    $file = self::$_classPath.
      str_replace(array('_', '\\'), DIRECTORY_SEPARATOR, $className).'.php';

    include $file;
  }
}