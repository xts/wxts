<?php
namespace Wxts;

/**
 * Represents a server with its own database and folders.
 */
class Server
{
  protected static $_activeServer;

  protected $_config = array(
    'db' => array
    (
      'host' => '127.0.0.1',
      'name' => 'changeit',
      'user' => 'user',
      'pass' => 'pass'
    ),
    'info' => array
    (
      'name' => null,
      'description' => null,
      'password' => null,
    ),
  );
  protected $_file;
  protected $_id;
  protected $_valid = null;

  public function __construct($name)
  {
    $this->_id = $name;

    $this->_file = realpath('config/').
      DIRECTORY_SEPARATOR.$this->_id.'.server.ini.php';
  }

  protected function _init()
  {
    // DataManager
    DataManager::getInstance()
      ->setDbDetails($this->getConfig('host', 'db'), $this->getConfig('user', 'db'),
        $this->getConfig('pass', 'db'), $this->getConfig('name', 'db'));

    // Check if connection details are valid
    try
    {
      DataManager::getInstance()->getDb();
    }
    catch (Exception $e)
    {
      $this->_valid = false;
    }
  }

  protected function _provideTmpFolder($folder)
  {
    // Directory exists?
    if (!is_dir($folder))
    {
      // Let's try to create it
      if (!mkdir($folder))
      {
        throw new Exception("Temporary folder is not writeable.");
      }
    }

    return $folder;
  }

  public static function getActiveServer()
  {
    if (null === self::$_activeServer)
    {
      self::$_activeServer = new Server(isset($_GET['server']) ?
        $_GET['server'] : null);
    }

    return self::$_activeServer;
  }

  public function getConfig($key, $section = 'info')
  {
    if (isset($this->_config[$section]) && isset($this->_config[$section][$key]))
    {
      return $this->_config[$section][$key];
    }

    return null;
  }

  public function getId()
  {
    return $this->_id;
  }

  public function getGraphPath($forLinks = false)
  {
    $folder = realpath(Controller::getConfig('tmpFolder')).
      DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;

    $this->_provideTmpFolder($folder);

    // No realpath if it should be used in links
    if ($forLinks)
    {
      $folder = Controller::getConfig('tmpFolder').
      'images'.DIRECTORY_SEPARATOR;
    }

    return $folder.$this->_id.'_';


  }

  public function getTmpPath()
  {
    $folder = realpath(Controller::getConfig('tmpFolder')).DIRECTORY_SEPARATOR;

    $this->_provideTmpFolder($folder);

    return $folder.$this->_id.'_';
  }

  /**
   * Returns whether server is valid
   */
  public function isValid()
  {
    if (null === $this->_valid)
    {
      $this->_valid = false;

      // Check whether ini for given server exists
      if (is_readable($this->_file))
      {
        $this->_config['info']['name'] = $this->_id;

        $this->_valid = true;

        $this->_config = array_merge($this->_config, parse_ini_file($this->_file, true));

        $this->_init();
      }
    }

    return $this->_valid;
  }

  public static function listServers()
  {
    $dir = dir(realpath('config'));
    $servers = array();

    // Loop configuration files
    while (false !== ($file = $dir->read()))
    {
      // If it's a configuration file
      if (substr($file, -15, 15) == '.server.ini.php')
      {
        $server = new Server(substr($file, 0, strlen($file) - 15));

        if ($server->isValid())
        {
          $servers[] = $server;
        }
      }
    }

    return $servers;
  }
}