<?php
namespace Wxts;

/**
 * The connection between DataManager and View and holder of (all)
 * application logic.
 * @author saviola
 */
class Controller extends \Lvc_PageController
{
  protected static $_activeController;
  protected static $_helperManager;


  public function __construct()
  {
    //parent::__construct();

    self::$_activeController = $this;
  }

  protected function afterAction()
  {
    self::$_helperManager->afterAction();
  }

  protected function beforeAction()
  {
    self::$_helperManager->beforeAction();
  }

  public function redirect($url)
  {
    $this->getHelperManager()->afterAction()->shutdown();

    parent::redirect($url);

    exit();
  }

  /**
   * Return a config value.
   */
  public static function getConfig($key, $section = 'misc')
  {
    return self::getHelperManager()->getHelper('config')->getConfig($key, $section);
  }

  public function getHelper($name)
  {
    return self::$_helperManager->getHelper($name);
  }

  public static function getHelperManager()
  {
    return self::$_helperManager;
  }

  public static function getInstance()
  {
    if (null == self::$_activeController)
    {
      throw new Exception("There is no active controller.");
    }

    return self::$_activeController;
  }

  public function getLayout()
  {
    return $this->layout;
  }

  public function getParam($key, $default = null)
  {
    if (isset($this->get[$key]))
    {
      return $this->get[$key];
    }
    else if (isset($this->post[$key]))
    {
      return $this->post[$key];
    }

    return $default;
  }

  public function requireCss($cssFile)
  {
    $this->layoutVars['requiredCss'][$cssFile] = true;
  }

  public function requireJs($jsFile)
  {
    $this->layoutVars['requiredJs'][$jsFile] = true;
  }

  public static function setHelperManager(Controller_Helper_Manager $helperManager)
  {
    self::$_helperManager = $helperManager;
  }
}