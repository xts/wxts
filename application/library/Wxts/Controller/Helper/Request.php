<?php
namespace Wxts;

/**
 * Helper to initialise a request object.
 */
class Controller_Helper_Request extends Controller_Helper_Abstract
{
  protected $request;

  public function init()
  {
    $this->_request = new Request();
  }

  public function getRequest()
  {
    return $this->_request;
  }
}