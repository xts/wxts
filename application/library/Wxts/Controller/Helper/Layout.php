<?php
namespace Wxts;

/**
 * Hepler to initialise layout variables.
 */
class Controller_Helper_Layout extends Controller_Helper_Abstract
{
  public function beforeAction()
  {
    $this->getController()->setLayout('default');

    $this->getController()->setLayoutVar('pageTitle', 'wXTS');
    //$this->requireCss('reset.css');
    $this->getController()->
      requireCss($this->getController()->getLayout().'.css');

    // Colour parser
    $cp = new \TMFColorParser();
    $this->getController()->setVar('colourParser', $cp);
  }
}