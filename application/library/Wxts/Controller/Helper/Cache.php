<?php
namespace Wxts;

/**
 * Helper which saves the cache on application shutdown.
 */
class Controller_Helper_Cache extends Controller_Helper_Abstract
{
  public function shutdown()
  {
    // Safe cache if neccessary
    Cache::save();
  }
}