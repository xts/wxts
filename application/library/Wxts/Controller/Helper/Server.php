<?php
namespace Wxts;

/**
 * Helper to determine which server to use.
 *
 * Redirects to the stats/form page, if no server was chosen or the form was
 * not submitted.
 */
class Controller_Helper_Server extends Controller_Helper_Abstract
{
  public function beforeAction()
  {
    $server = Server::getActiveServer();

    // If no valid server is selected and the server choosing page was not
    // requested, redirect to it
    if ((!$server->isValid() || !isset($_REQUEST['form']))
      && ($this->getController()->getControllerName() == 'stats'
        && $this->getController()->getActionName() != 'form'))
    {
      $this->getController()->redirect('?c=stats&a=form');
    }
  }
}