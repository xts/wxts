<?php
namespace Wxts;

/**
 * Abstract implementation of a controller helper.
 *
 * Provides hooks to execute code before and after actions.
 */
abstract class Controller_Helper_Abstract implements Controller_Helper_Interface
{
  public function __construct()
  {
    $this->init();
  }

  /**
   * Empty implementations of required methods.
   */
  public function afterAction(){}

  /**
   * Empty implementations of required methods.
   */
  public function beforeAction() {}

  public function getController()
  {
    return Controller::getInstance();
  }

  /**
   * Empty implementations of required methods.
   */
  public function init() {}

  public function shutdown() {}
}