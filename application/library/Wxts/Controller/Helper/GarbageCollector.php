<?php
namespace Wxts;

/**
 * Helper to initialise the garbage collector and execute it.
 */
class Controller_Helper_GarbageCollector extends Controller_Helper_Abstract
{
  public function init()
  {
    // Run garbage collector early to avoid surprises involing missing files
    $gc = new GarbageCollector(Controller::getConfig('retentionTime', 'gc')*60*60*24,
      Controller::getConfig('probability', 'gc'));
    $gc->run(realpath(Controller::getConfig('tmpFolder')));

    // Get last garbage collection time
    $gcTime = @filemtime(realpath(Controller::getConfig('tmpFolder')).
        DIRECTORY_SEPARATOR.'gc');
    $gcTime = $gcTime ? date('d.m.Y H:i:s', $gcTime) : "never";
  }
}