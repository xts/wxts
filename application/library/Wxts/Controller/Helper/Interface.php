<?php
namespace Wxts;

/**
 * Interface for controller helpers.
 */
interface Controller_Helper_Interface
{
  public function afterAction();

  public function beforeAction();

  public function getController();

  public function init();

  public function shutdown();
}