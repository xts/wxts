<?php
namespace Wxts;

/**
 * Manager for all helpers which provides the hooks to be executed on all
 * registered helpers.
 */
class Controller_Helper_Manager
{
  protected $_helpersNamed = array();
  protected $_helpersNumbered = array();

  /**
   * Trigger afterAction() method on all registered helpers.
   *
   * Works in reversed order.
   */
  public function afterAction()
  {
    for ($i = count($this->_helpersNumbered) -1; $i >= 0; $i--)
    {
      $this->_helpersNumbered[$i]->afterAction();
    }

    return $this;
  }

  /**
   * Add a single helper.
   */
  public function addHelper(Controller_Helper_Interface $helper)
  {
    // Helper name is the last part of the class name
    $a = explode('_', get_class($helper));
    $name = lcfirst(array_pop($a));

    $this->_helpersNamed[$name] = $helper;
    $this->_helpersNumbered[] = $helper;

    return $this;
  }

  /**
   * Add array of helpers
   *
   * @param $helpers array of objects or strings or a mixture of both
   */
  public function addHelpers(array $helpers)
  {
    foreach ($helpers as $helper)
    {
      // Convert string to class name
      if (is_string($helper))
      {
        $class = "Wxts\Controller_Helper_" . $helper;

        if (!class_exists($class))
        {
          throw new Exception("Invalid helper class \"$class\"");
        }

        $helper = new $class();
      }

      $this->addHelper($helper);
    }

    return $this;
  }

  /**
   * Trigger beforeAction() method on all registered helpers.
   */
  public function beforeAction()
  {
    foreach ($this->_helpersNamed as $helper)
    {
      $helper->beforeAction();
    }

    return $this;
  }

  public function getHelper($name)
  {
    if (!isset($this->_helpersNamed[$name]))
    {
      throw new Exception("Unknown helper \"$name\"");
    }

    return $this->_helpersNamed[$name];
  }

  /**
   * Trigger shutdown() method on all registered helpers.
   *
   * Works in reversed order.
   */
  public function shutdown()
  {
    for ($i = count($this->_helpersNumbered) -1; $i >= 0; $i--)
    {
      $this->_helpersNumbered[$i]->shutdown();
    }

    return $this;
  }
}