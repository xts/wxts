<?php
namespace Wxts;

/**
 * Helper to manage the configuration.
 */
class Controller_Helper_Config extends Controller_Helper_Abstract
{
  protected $_config;

  public function init()
  {
    // Load config
    if (null === $this->_config)
    {
      $file = realpath('config/').DIRECTORY_SEPARATOR.'config.ini.php';

      if (is_readable($file))
      {
        $this->_config = parse_ini_file($file, true);
      }
      else
      {
        throw new \Exception('Unable to read configuration file.');
      }
    }

    // Add pathes for jpgraph
    set_include_path(get_include_path().PATH_SEPARATOR.CLASS_PATH.'jpgraph'.
      PATH_SEPARATOR.$this->getConfig('path', 'jpgraph'));
  }

  public function getConfig($key, $section = 'misc')
  {
    if (isset($this->_config[$section])
    && isset($this->_config[$section][$key]))
    {
      return $this->_config[$section][$key];
    }

    return null;
  }
}