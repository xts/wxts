<?php
namespace Wxts;

/**
 * Simple class representing a map.
 */
class Map extends Entity
{
  public function setName($name)
  {
    parent::setName(self::stripTags($name));
  }
}