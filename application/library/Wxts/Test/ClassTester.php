<?php
namespace Wxts;

class Test_ClassTester
{
  public function __construct($rootDir)
  {
    $this->_checkDir($rootDir);
  }

  protected function _checkDir($dir)
  {
    $d = dir($dir);

    while (false !== ($entry = $d->read()))
    {
       if (substr($entry, 0, 1) == '.')
       {
         continue;
       }

       if (is_dir($dir.DIRECTORY_SEPARATOR.$entry))
       {
         $this->_checkDir($dir.DIRECTORY_SEPARATOR.$entry);
       }
       else
       {
         include_once $dir.DIRECTORY_SEPARATOR.$entry;
       }
    }

    $d->close();
  }
}