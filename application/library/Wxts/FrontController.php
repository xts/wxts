<?php
namespace Wxts;

/**
 * Extended front controller.
 */
class FrontController extends \Lvc_FrontController
{
  protected $_helperManager;

  public function __construct()
  {
    // Create helper manager
    $this->_helperManager = new Controller_Helper_Manager();

    Controller::setHelperManager($this->_helperManager);

    // Add various helpers
    // @TODO Move this to config?
    // Server has to be last, because it can trigger a redirect
    $this->_helperManager->
      addHelpers(array('Config', 'GarbageCollector', 'Layout', 'Request', 'Server'));
  }

  public function processRequest(\Lvc_Request $request)
  {
    // First let the parent Front controller process the request
    parent::processRequest($request);

    // Now we can shutdown all helpers
    $this->_helperManager->shutdown();
  }
}