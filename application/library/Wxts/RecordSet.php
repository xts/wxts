<?php
namespace Wxts;

/**
 * A RecordSet represents multiple instances of the class Record.
 */
class RecordSet extends Record
{
  protected $_hasRecordSets = false;
  protected $_records = array();
  protected $_sortField = "> unsorted";

  public function __construct()
  {}

  public function addRecord($record)
  {
    // RecordSet takes identity of first added record
    // Only to return sorting fields player, map and date
    if (empty($this->_records) &&
      // Empty recordsets can't be used to fill this recordset with data
      (!($record instanceof RecordSet) || $record->getCount() > 0))
    {
      parent::__construct(0, $record->getPlayer(),
        $record->getMap(), array(), 0,
        $record->getdate(), $record->getMode());
    }

    if (($id = $record->getId()) == 0)
    {
      $this->_hasRecordSets = true;
      $this->_records[] = $record;
    }
    else
    {
      $this->_records[$id] = $record;
    }

    return $this;
  }

  public function addRecords(array $records)
  {
    foreach ($records as $record)
    {
      $this->addRecord($record);
    }

    return $this;
  }

  public function getCount()
  {
    return count($this->_records);
  }

  public function getRecord($id)
  {
    if (!$this->hasRecord($id))
    {
      throw new Exception('RecordSet does not have Record with id '.$id);
    }

    return $this->_records[$id];
  }

  public function getRecords()
  {
    return $this->_records;
  }

  public function getSortFields() { return $this->_sortFields; }

  public function hasRecord($id)
  {
    return isset($this->_records[$id]);
  }

  public function hasRecordSets()
  {
    return (bool) $this->_hasRecordSets;
  }

  /**
   * Return all times of all records and RecordSets within this RecordSet
   *
   * @see Record::getTimes()
   */
  public function getTimes()
  {
    $times = array();

    foreach ($this->_records as $record)
    {
      $times = array_merge($times, $record->getTimes());
    }

    return $times;
  }

  public function sort(Record_Sorter_Abstract $sorter)
  {
    usort($this->_records, array($sorter, 'compare'));

    $this->_sortFields = $sorter->getSortFields();
  }

  /**
   * Returns an array for debugging purposes.
   *
   * The array contains the sort field and all contained records.
   */
  public function toArray($field = null)
  {
    // Sort field
    $array = array('__sortField' => $this->_sortField);

    if (null !== $field)
    {
      $array['__value'] = $this->getSortFieldIdentifier(true, false, $field);
    }

    // All records
    foreach ($this->_records as $record)
    {
        $array[] = $record->toArray($this->_sortField);
    }

    return $array;
  }
}