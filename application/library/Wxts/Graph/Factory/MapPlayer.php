<?php

namespace Wxts;

require_once("pchart/class/pData.class.php");
require_once("pchart/class/pDraw.class.php");
require_once("pchart/class/pImage.class.php");

class Graph_Factory_MapPlayer extends Graph_Factory_Abstract
{
  private function _buildSeries($recordSet)
  {
    $series = array();
    $series['time'] = array();
    $series['average'] = array();
    $series['fails'] = array();

    foreach ($recordSet->getRecords() as $record)
    {
      $series['time'] = array_merge($series['time'], $record->getTimes());

      $series['fails'] = array_merge($series['fails'],
      array_fill(0, count($record->getTimes()),
      $record->getFails(true)));
    }

    $countTimes = count($series['time']);

    for ($i = 1; $i < $countTimes; $i++)
    {
      if ($i % 2 == 1)
      {
        $series['average'][$i] =
        ($series['time'][$i-1] +
        $series['time'][$i] +
        (isset($series['time'][$i+1]) ? $series['time'][$i+1] : $series['time'][$i]))
        /3;
      }
      else
      {
        $series['average'][$i] = VOID;
      }
    }

    if (count($series['average']) == 0)
    {
      $series['average'][0] = $series['time'][0];
    }
    else if ($series['average'][$countTimes-1] == VOID)
    {
      $series['average'][] =
      ($series['time'][$countTimes-2] +
      2*$series['time'][$countTimes-1])/3;
    }

    return $series;
  }

  public function buildGraphs(RecordSet $recordSet)
  {
    $series = $this->_buildSeries($recordSet);

    $myData = new \pData();

    $myData->addPoints($series['time'],"times");
    $myData->setSerieDescription("times","Times");
    $myData->setSerieOnAxis("times",0);

    $myData->addPoints($series['average'],"average");
    $myData->setSerieDescription("average","Avg. times");
    $myData->setSerieOnAxis("average",0);

    $myData->addPoints($series['fails'],"fails");
    $myData->setSerieDescription("fails","Fails");
    $myData->setSerieOnAxis("fails",1);

    $myData->setAxisPosition(0,AXIS_POSITION_LEFT);
    $myData->setAxisName(0,"Times");
    $myData->setAxisUnit(0,"");

    $myData->setAxisPosition(1,AXIS_POSITION_RIGHT);
    $myData->setAxisName(1,"Fails");
    $myData->setAxisUnit(1,"%");

    $myPicture = new \pImage(800,250,$myData);
    $myPicture->Antialias = false;

    $Settings = array("R"=>41, "G"=>207, "B"=>201, "Dash"=>1, "DashR"=>61, "DashG"=>227, "DashB"=>221);
    $myPicture->drawFilledRectangle(0,0,800,250,$Settings);

    $myPicture->drawRectangle(0,0,799,249,array("R"=>0,"G"=>0,"B"=>0));

    //$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>20));

    $myPicture->setFontProperties(array("FontName"=>CLASS_PATH."pchart/fonts/verdana.ttf","FontSize"=>14));
    $TextSettings = array("Align"=>TEXT_ALIGN_MIDDLEMIDDLE
    , "R"=>0, "G"=>0, "B"=>0);
    $myPicture->drawText(350,25,$recordSet->getPlayer()->getName(). " on ".
    html_entity_decode($recordSet->getMap()->getName()),$TextSettings);

    $myPicture->setShadow(FALSE);
    $myPicture->setGraphArea(50,50,645,190);
    $myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>CLASS_PATH."pchart/fonts/verdana.ttf","FontSize"=>6));

    $Settings = array("Pos"=>SCALE_POS_LEFTRIGHT
    , "Mode"=>SCALE_MODE_FLOATING
    , "LabelingMethod"=>LABELING_ALL
    , "GridR"=>255, "GridG"=>255, "GridB"=>255, "GridAlpha"=>50, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>0, "LabelSkip"=>1, "CycleBackground"=>1, "DrawXLines"=>1, "DrawSubTicks"=>1, "SubTickR"=>255, "SubTickG"=>0, "SubTickB"=>0, "SubTickAlpha"=>50, "DrawYLines"=>ALL);
    $myPicture->drawScale($Settings);

    //$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10));

    $Config = array('BreakVoid' => false);
    $myPicture->drawSplineChart($Config);

    $Config = array("FontR"=>0, "FontG"=>0, "FontB"=>0, "FontName"=>CLASS_PATH."pchart/fonts/verdana.ttf", "FontSize"=>6, "Margin"=>6, "Alpha"=>30, "BoxSize"=>5, "Style"=>LEGEND_NOBORDER
    , "Mode"=>LEGEND_HORIZONTAL
    );
    $myPicture->drawLegend(616,16,$Config);

    return array($myPicture);
  }

  public function getNumberOfGraphs()
  {
    return 1;
  }
}