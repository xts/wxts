<?php
namespace Wxts;

/**
 * Null-factory. Will just create nothing.
 */
class Graph_Factory_Null extends Graph_Factory_Abstract
{
  public function buildGraphs(RecordSet $recordSet)
  {
    return array();
  }

  public function getNumberOfGraphs()
  {
    return 0;
  }
}