<?php
namespace Wxts;

abstract class Graph_Factory_Abstract
{
  /**
   * Build graphs and return them as an array.
   */
  abstract public function buildGraphs(RecordSet $recordSet);

  /**
   * Simply return the number of graphs created by this factory.
   */
  abstract public function getNumberOfGraphs();
}
