<?php
namespace Wxts;

/**
 * Manages all graphs and provides public interface.
 */
class Graph_Manager
{
  protected $_cache;
  protected $_factory;
  protected $_hash;

  protected static $_instance;

  protected function __construct()
  {
    $this->_cache = new Graph_Cache();
    $this->_factory = new Graph_Factory_Null();
  }


  protected function _buildFactory($className)
  {
    if (class_exists($className))
    {
      return new $className();
    }

    throw new \Exception('Invalid factory "' . $className . '"');
  }

  public function getFactory()
  {
    return $this->_factory;
  }

  public function getGraphUrls(RecordSet $recordSet)
  {
    if ($this->_factory->getNumberOfGraphs() == 0)
    {
      return array();
    }

    $hashCode = $this->_hash->getHashCode($recordSet);

    if (!$this->_cache->hasGraphs($hashCode))
    {
      $this->_cache->saveGraphs($hashCode, $this->_factory->buildGraphs($recordSet));
    }

    return $this->_cache->getGraphUrls($hashCode, $this->_factory->getNumberOfGraphs());
  }

  public static function getInstance()
  {
    if (null == self::$_instance)
    {
      self::$_instance = new Graph_Manager();
    }

    return self::$_instance;
  }

  /**
   * Sets up the currently used factory.
   */
  public function setUpFactory(Record_Hash_Abstract $hash)
  {
    $this->_hash = $hash;
    $groupFields = $hash->getHashFields();
    sort($groupFields);

    $factory = null;
    $factoryClassName = '';
    $factoryClassNamePrefix = 'Wxts\\Graph_Factory_';

    while (!empty($groupFields) && null == $factory)
    {
      try
     {
        $factory = $this->_buildFactory($factoryClassNamePrefix.
          implode('', $groupFields));
      }
      catch (\Exception $e)
      {
        array_pop($groupFields);
      }
    }

    $this->_factory = $factory ? $factory : new Graph_Factory_Null();
  }
}