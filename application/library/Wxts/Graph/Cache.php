<?php
namespace Wxts;

/**
 * Cache for graphs.
 */
class Graph_Cache
{
  protected function _getFileName($hashCode, $suffix = "_0", $forLink = false)
  {
    $filename = Server::getActiveServer()->getGraphPath($forLink). $hashCode . $suffix .'.png';

    return $filename;
  }

  public function getGraphUrls($hashCode, $num)
  {
    $urls = array();
    $suffix = null;

    for ($i = 0; $i < $num; $i++)
    {
      $suffix = '_' . $i;
      $urls[] = $this->_getFileName($hashCode, $suffix, true);
    }

    return $urls;
  }

  public function hasGraphs($hashCode)
  {
    $filename = $this->_getFileName($hashCode);

    // Check whether graph for given hash exists
    if (file_exists($filename))
    {
      return true;
    }

    return false;
  }

  /**
   * Saves graphs to file
   */
  public function saveGraphs($hashCode, $graphs)
  {
    $i = 0;
    $suffix = null;
    foreach ($graphs as $graph)
    {
      $suffix = '_' . $i;
      $filename = $this->_getFileName($hashCode, $suffix);

      $graph->render($filename);

      $i++;
    }
  }
}