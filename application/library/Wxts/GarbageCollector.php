<?php
namespace Wxts;

/**
 * Simple Garbage collector to delete old files in temporary folder
 */
class GarbageCollector
{
  protected $_retentionTime;
  protected $_probability;

  public function __construct($retentionTime, $probability = 1)
  {
    $this->_retentionTime = $retentionTime;
    $this->_probability = $probability;
  }

  protected function _collectGarbage($folder)
  {
    if (!is_writable($folder))
    {
      throw new Exception("Error during garbage collection: Folder not writable.");
    }

    $dir = dir($folder);
    $folder .= DIRECTORY_SEPARATOR;

    $maxAge = time() - $this->_retentionTime;

    // Loop temporary files
    while (false !== ($file = $dir->read()))
    {
      // If it's a dir, dive into it
      if (is_dir($folder.$file) && substr($file, 0, 1) != '.')
      {
        $this->_collectGarbage($folder.$file);
      }

      // If file is older than time - retentionTime
      else if (filemtime($folder.$file) < $maxAge)
      {
        unlink($folder.$file);
      }
    }
  }

  public function run($folder)
  {
    $rnd = mt_rand(1, 100);

    if ($rnd <= $this->_probability)
    {
      $this->_collectGarbage($folder);
      touch($folder.DIRECTORY_SEPARATOR.'gc');
      return true;
    }

    return false;
  }
}