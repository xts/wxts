<?php
namespace Wxts;

/**
 * Simple Cache for users and maps to minimize database access.
 *
 * Object will be serialized and stored in ./cache.
 *
 * @author saviola
 */
class Cache
{
  protected static $_instance;

  /**
   * Whether objects have been added to the cache
   * @var bool
   */
  protected $_dirty = false;
  protected $_maps = array();
  protected $_players = array();

  protected static function _loadCache()
  {
    $file = self::getCacheFile();

    // If file exists and is readable
    if (is_readable($file))
    {
      $cache = @unserialize(file_get_contents($file));

      // If object is corrupted
      if (!is_object($cache))
      {
        return false;
      }

      return $cache;
    }

    return false;
  }

  public function __construct()
  {
    $this->_dirty = true;
  }

  public function __sleep()
  {
    return array('_maps', '_players');
  }

  public static function getCacheFile()
  {
    return Server::getActiveServer()->getTmpPath().'cache';
  }

  public static function getInstance()
  {
    if (null === self::$_instance)
    {
      $loadedCache = self::_loadCache();

      self::$_instance = $loadedCache instanceof Cache ? $loadedCache : new self();
    }

    return self::$_instance;
  }

  /**
   * Adds maps and players to the cache object
   * @param array $results
   */
  public function add(Entity $object)
  {
    if ($object instanceof Map)
    {
      $this->_maps[$object->getId()] = $object;
      $this->_maps[$object->getName()] = $object;
    }
    else
   {
      $this->_players[$object->getId()] = $object;
      $this->_players[$object->getName()] = $object;
    }

    $this->_dirty = true;

    return $this;
  }

  public function getMap($id)
  {
    if (!$this->hasMap($id))
    {
      throw new Exception('No map with given id '.$id.' found.');
    }

    return $this->_maps[$id];
  }

  public function getPlayer($id)
  {
    if (!$this->hasPlayer($id))
    {
      throw new Exception('No player with given id '.$id.' found.');
    }

    return $this->_players[$id];
  }


  public function hasMap($id)
  {
    return (isset($this->_maps[$id]));
  }

  public function hasPlayer($id)
  {
    return (isset($this->_players[$id]));
  }

  public function isDirty()
  {
    return $this->_dirty;
  }

  public static function save()
  {
    $cache = self::getInstance();
    $server = Server::getActiveServer();

    if (!$cache->isDirty() || !$server->isValid())
    {
      return;
    }

    $serialized = serialize($cache);

    @file_put_contents($server->getTmpPath().'cache', $serialized);
  }
}