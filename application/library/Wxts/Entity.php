<?php
namespace Wxts;

/**
 * Minimal XAseco-Entity, consisting of an id and a name.
 *
 * Provides a function for formatting Names containg TM format strings ($xzy).
 * They are not parsed but rather removed.
 */
abstract class Entity
{
  protected $_id;
  protected $_name;

  // Color parser, used to strip names from color tags and stuff
  protected static $_cp;

  public function __construct($id, $name = null)
  {
    $this->_id = $id;
    $this->setName($name);
  }

  public static function getCp()
  {
    if (null == self::$_cp)
    {
      self::$_cp = new \TMFColorParser();
    }

    return self::$_cp;
  }

  public function getId() { return $this->_id; }
  public function getName() { return $this->_name; }


  public function setName($name)
  {
    $this->_name = (string) $name;
  }

  /**
   * Strip Trackmania tags from given text
   */
  public static function stripTags($text)
  {
    return self::getCp()->toHTML($text, true, true, 'all');
  }
}