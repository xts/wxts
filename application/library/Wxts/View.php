<?php
namespace Wxts;

/**
 * Extended version of Lvc_View.
 */
class View extends \Lvc_View
{
	public function requireCss($cssFile)
	{
		$this->controller->requireCss($cssFile);
	}
}

?>