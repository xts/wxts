<?php
namespace Wxts;
class Record_Sorter_AverageTime extends Record_Sorter_Abstract
{
  public function compare(Record $rec1, Record $rec2)
  {
    return $this->_compare($rec1->getAverageTime(true), $rec2->getAverageTime(true));
  }

  public function getSortFields()
  {
    return array('average time');
  }
}