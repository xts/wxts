<?php
namespace Wxts;

class Record_Sorter_Date extends Record_Sorter_Abstract
{
  public function compare(Record $rec1, Record $rec2)
  {
    return $this->_compare($rec2->getDate(true), $rec1->getDate(true));
  }
}