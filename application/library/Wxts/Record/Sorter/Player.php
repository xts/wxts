<?php
namespace Wxts;

class Record_Sorter_Player extends Record_Sorter_Abstract
{
  public function compare(Record $rec1, Record $rec2)
  {
    return strcmp
      ($rec1->getPlayer()->getName(), $rec2->getPlayer()->getName());
  }
}