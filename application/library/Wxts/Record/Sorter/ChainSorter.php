<?php
namespace Wxts;

/**
 * Class that represents a chain sorter, which allows for serveral sort
 * fields.
 */
class Record_Sorter_ChainSorter extends Record_Sorter_Abstract
{
  protected $_sorters = array();

  public function addSorter(Record_Sorter_Abstract $sorter)
  {
    $this->_sorters[] = $sorter;
  }

  public function getSortFields()
  {
    $fields = array();

    foreach ($this->_sorters as $sorter)
    {
      $fields = array_merge($fields, $sorter->getSortFields());
    }

    return $fields;
  }

  public function compare(Record $r1, Record $r2)
  {
    $count = count($this->_sorters);
    $sorter = null;
    $result = 0;

    // While the result is 0, move through all sorters
    for($i = 0; ($i < $count) && ($result == 0); $i++)
    {
      $result = $this->_sorters[$i]->compare($r1, $r2);
    }

    return $result;
  }
}