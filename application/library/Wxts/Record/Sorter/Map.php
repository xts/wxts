<?php
namespace Wxts;

class Record_Sorter_Map extends Record_Sorter_Abstract
{
  public function compare(Record $rec1, Record $rec2)
  {
    return strcmp
      ($rec1->getMap()->getName(), $rec2->getMap()->getName());
  }
}