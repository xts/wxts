<?php
namespace Wxts;

/**
 * Parent class for all record-comparing classes.
 */
abstract class Record_Sorter_Abstract
{
  protected function _compare($v1, $v2)
  {
    if ($v1 > $v2)
    {
      return 1;
    }
    else if ($v1 < $v2)
    {
      return -1;
    }
    else
    {
      return 0;
    }
  }

  public function getSortFields()
  {
    $classNameArray = explode('_', get_called_class());

    return array(array_pop($classNameArray));
  }

  abstract public function compare(Record $rec1, Record $rec2);
}