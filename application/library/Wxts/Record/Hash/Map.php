<?php
namespace Wxts;

class Record_Hash_Map extends Record_Hash_Abstract
{
  public function getHashCode(Record $rec)
  {
    return $rec->getMap()->getId();
  }

  public function getHashFieldIdentifiers(Record $rec)
  {
    return array($rec->getMap()->getName());
  }

  public function getHashFields()
  {
    return array('Map');
  }
}