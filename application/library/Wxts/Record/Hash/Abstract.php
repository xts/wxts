<?php
namespace Wxts;

/**
 * A hash creates a hash code for a given record.
 */
abstract class Record_Hash_Abstract
{
  /**
   * This method should return a not empty string representing a hash for the
   * given record.
   */
  public abstract function getHashCode(Record $rec);

  /**
   * This method should return an array of values of the hash fields used for
   * creating the hash of the given record.
   */
  public abstract function getHashFieldIdentifiers(Record $rec);

  /**
   * This method should return an array of fields used to determine the hash of
   * a record.
   */
  public abstract function getHashFields();
}