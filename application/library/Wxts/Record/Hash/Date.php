<?php
namespace Wxts;

class Record_Hash_Date extends Record_Hash_Abstract
{
  public function getHashCode(Record $rec)
  {
    return date('dmy', $rec->getDate());
  }

  public function getHashFieldIdentifiers(Record $rec)
  {
    return array(date('d.m.y', $rec->getDate()));
  }

  public function getHashFields()
  {
    return array('Date');
  }
}