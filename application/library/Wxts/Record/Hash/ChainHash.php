<?php
namespace Wxts;

class Record_Hash_ChainHash extends Record_Hash_Abstract
{
  protected $_hashes = array();

  public function addHash(Record_Hash_Abstract $hash)
  {
    $this->_hashes[implode('', $hash->getHashFields())] = $hash;
  }

  public function getHashCode(Record $rec)
  {
    $hashCode = 'H'; // To avoid empty string index problems

    foreach ($this->_hashes as $hash)
    {
      $hashCode .= $hash->getHashCode($rec);
    }

    return $hashCode;
  }

  public function toString(Record $rec)
  {
    return $this->getHashCode($rec);
  }

  public function getHashFieldIdentifiers(Record $rec)
  {
    $identifiers = array();

    foreach ($this->_hashes as $hash)
    {
      $identifiers =
        array_merge($identifiers, $hash->getHashFieldIdentifiers($rec));
    }

    return $identifiers;
  }

  public function getHashFields()
  {
    $fields = array();

    foreach ($this->_hashes as $hash)
    {
      $fields = array_merge($fields, $hash->getHashFields());
    }

    return $fields;
  }

  public function sort()
  {
    ksort($this->_hashes);

    return $this;
  }
}