<?php
namespace Wxts;

class Record_Hash_Player extends Record_Hash_Abstract
{
  public function getHashCode(Record $rec)
  {
    return $rec->getPlayer()->getId();
  }

  public function getHashFieldIdentifiers(Record $rec)
  {
    return array($rec->getPlayer()->getName());
  }

  public function getHashFields()
  {
    return array('Player');
  }
}