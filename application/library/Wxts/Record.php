<?php
namespace Wxts;

/**
 * Represents the basic entity of the application: A single record.
 *
 * A record consists of:
 *
 * - A player name
 * - A map name
 * - an average time
 * - a number of attempted runs
 * - a number of fails (absolute and percentaged)
 *
 * Optionally there is a verbose format of a record which displays all
 * driven times.
 */
class Record
{
  public static $modes = array("RN", "TA", "TM", "LP", "ST", "CP");

  protected $_average;
  protected $_count;
  protected $_date;
  protected $_dateToDay; // Only ymd format
  protected $_fails;
  protected $_id;
  protected $_map;
  protected $_mode = 0;
  protected $_player;
  protected $_times = array();


  public function __construct($id, Player $player, Map $map, array $times, $count, $date, $mode)
  {
    $this->_id = $id;
    $this->_player = $player;
    $this->_map = $map;
    $this->_times = $times;
    $this->_count = $count;
    $this->_date = is_numeric($date) ? $date : strtotime($date);
    $this->_dateToDay = date('ymd', $this->_date);
    $this->_mode = $mode;
  }

  public function getId() { return $this->_id; }
  public function getDate($ymdFormat = false)
  {
    return $ymdFormat ? $this->_dateToDay : $this->_date;
  }
  public function getPlayer() { return $this->_player; }
  public function getMap() { return $this->_map; }
  public function getMode() { return $this->_mode; }
  public function getTimes() { return $this->_times; }
  public function getCount() { return $this->_count > 0 ? $this->_count : 1; }

  public function getAverageTime($numeric = false)
  {
    $count = count($this->_times);
    $count = $count > 0 ? $count : 1;

    $average = ($this->_average ? $this->_average :
      ($this->_average = round(array_sum($this->_times)/$count, 2)));

    if (!$numeric)
    {
      // formats to 01:05:50
      $average = TimeFormatter::asString($average);
    }

    return $average;
  }

  public function getFails($percent = false)
  {
    if (null === $this->_fails)
    {
      $this->_fails = $this->_count-count($this->_times);
    }

    return ($percent ? round($this->_fails/($this->getCount()/100), 1)
      : $this->_fails);
  }

  public function toArray($field = null)
  {
    return array($field ? $this->getSortFieldIdentifier($field) : 'unsorted');
  }
}