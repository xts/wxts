<?php
namespace Wxts;

/**
 * This class provides a mechanism to sort and group records.
 */
class Sequencer
{
  protected $_groupHash;
  protected $_recordSet;
  protected $_sorter;

  /**
   * Group records into record sets for each combination of values for the given
   * fields
   *
   * @param RecordSet $recordSets
   * @param array $sortFields
   * @throws Exception
   */
  protected function _group($groupFields)
  {
    $groupFields = (array) $groupFields;

    // Build the hash chain
    $hashChain = new Record_Hash_ChainHash();

    $classNamePrefix = 'Wxts\\Record_Hash_';
    foreach ($groupFields as $field)
    {
      $hashClassName = $classNamePrefix.ucfirst($field);

      if (class_exists($hashClassName))
      {
        $hashChain->addHash(new $hashClassName());
      }
    }

    $this->_groupHash = $hashChain;

    // Resulting record set
    $recordSetResult = new RecordSet();
    // Associative array, the keys are the hash codes
    $recordSets = array();

    $hashCode = "";
    foreach ($this->_recordSet->getRecords() as $record)
    {
      $hashCode = $hashChain->getHashCode($record);

      // No group for this hash code exists
      if (!isset($recordSets[$hashCode]))
      {
        // Create the group
        $recordSets[$hashCode] = new RecordSet();

        // And add it to the result record set
        $recordSetResult->addRecord($recordSets[$hashCode]);
      }

      $recordSets[$hashCode]->addRecord($record);
    }

    $this->_recordSet = $recordSetResult;
  }

  protected function _sort(array $sortFields)
  {
    $sortFields = (array) $sortFields;

    $classNamePrefix = "Wxts\\Record_Sorter_";
    $chainSorter = new Record_Sorter_ChainSorter();

    foreach ($sortFields as $field)
    {
      if (class_exists(($sorterClassName = $classNamePrefix.ucfirst($field))))
      {
        $chainSorter->addSorter(new $sorterClassName());
      }
    }

    $this->_sorter = $chainSorter;

    $this->_recordSet->sort($chainSorter);
  }

  public function __construct(RecordSet $recordSet)
  {
    $this->_recordSet = $recordSet;
  }

  public function getGroupHash()
  {
    return $this->_groupHash;
  }

  public function getRecordSet($sortFields = null, $groupFields = null)
  {
    if (null != $sortFields)
    {
      $this->_sort($sortFields);
    }

    if (null != $groupFields)
    {
      $this->_group($groupFields);
    }

    return $this->_recordSet;
  }

  public function getSorter()
  {
    return $this->_sorter;
  }
}