<?php
namespace Wxts;

/**
 * Manages all request data like get- and post-data.
 */
class Request
{
  protected $_data = array
    (
      'group' => array('date', 'map', ''),
    	'maps' => array(),
      'mode' => "",
      'password' => '',
    	'players' => array(),
    	'server' => null,
      'sort' => array('date', 'map', 'averageTime'),
    );

  public function __construct()
  {
    // Merge our request data
    $this->_data = array_merge($this->_data, array_intersect_key($_REQUEST, $this->_data));

    $this->_parseRequest();
  }

  public function getData($field, $default = false)
  {
    return isset($this->_data[$field]) ? $this->_data[$field] : $default;
  }

  protected function _parseRequest()
  {
    // Parse player logins
    if (!is_array($this->_data['players']))
    {
      $this->_data['players'] = array_filter(explode(',', $this->_data['players']));
    }

    // Parse map IDs
    if (!is_array($this->_data['maps']))
    {
      $this->_data['maps'] = array_filter(explode(',', $this->_data['maps']));
    }

    // Sort group fields alphabetically
    sort($this->_data['group']);
  }

  public function toArray()
  {
    return $this->_data;
  }
}