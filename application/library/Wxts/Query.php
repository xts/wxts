<?php
namespace Wxts;

/**
 * Class representing an sql query.
 */
class Query
{
  protected $_select =
    "SELECT xts.*,(UNIX_TIMESTAMP(xts.Timestamp)) AS date, p.Login, p.NickName, c.Name as ChallengeName
     FROM xts, players as p, challenges as c";
  protected $_where = "WHERE p.Id = xts.PlayerId AND c.Id = xts.ChallengeId ";

  protected $_order = null;

  public function __construct(array $data)
  {
    // Players
    if (!empty($data['players']))
    {
      $this->addWhere('p.login', $data['players']);
    }

    // Maps
    if (!empty($data['maps']))
    {
      $this->addWhere('xts.ChallengeId', $data['maps']);
    }

    /* Modes
    if (!empty($data['modes']))
    {
      $this->addWhere('xts.Mode', $data['modes']);
    }*/

    $this->_order = 'ORDER BY id DESC LIMIT '.
      Controller::getConfig('maxDisplayCount');
  }

  /**
   * Extend where clause.
   *
   * @param $key database field, will not be escaped!
   * @param $value Value(s) of the field, will be escaped
   * @param $prefix Wheter or not the new clause should be added with a
   * prefix
   */
  public function addWhere($key, $value, $prefix = "AND")
  {
    $this->_where .= $prefix." ( ";

    if (is_array($value))
    {
      $tPrefix = "";

      foreach ($value as $tValue)
      {
        $this->addWhere($key, $tValue, $tPrefix);

        if (!$tPrefix)
        {
          $tPrefix = "OR";
        }
      }
    }
    else
    {
      $this->_where .= $key."='".mysql_real_escape_string($value)."' ";
    }

    $this->_where .= " ) ";
  }

  public function getSql()
  {
    $query = $this->_select . " " . $this->_where . " " . $this->_order;

    return $query;
  }
}