<?php
namespace Wxts;

/**
 * Simple class representing a player.
 */
class Player extends Entity
{
  protected $_nickname;

  public function __construct($id, $name = null, $nickname = null)
  {
    parent::__construct($id, $name);

    $this->setNickname($nickname);
  }

  public function getNickname()
  {
    return $this->_nickname;
  }

  public function setNickname($nickname)
  {
    $this->_nickname = self::stripTags($nickname);
  }
}