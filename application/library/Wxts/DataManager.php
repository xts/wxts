<?php
namespace Wxts;

/**
 * One and only accessor of the database connection.
 */
class DataManager
{
  protected static $_instance;

  protected $_cache;
  protected $_dbDetails = array();
  protected $_db;
  protected $_initialized = false;

  /**
   * Transforms row object into Records and returns a RecordSet.
   */
  protected function _processRecords($data)
  {
    $recordSet = new RecordSet();

    foreach ($data as $row)
    {
      $player = $this->getPlayer($row->PlayerId, $row->Login, $row->NickName);
      $map = $this->getMap($row->ChallengeId, $row->ChallengeName);

      $recordSet->addRecord(
        new Record($row->id, $player, $map, explode(',', $row->Times),
        $row->Count, $row->date, isset($row->Mode) ? $row->Mode : 0)
      );
    }

    return $recordSet;
  }

  protected function __construct()
  {
    $this->_cache = Cache::getInstance();
  }

  public static function getInstance()
  {
    if (null === self::$_instance)
    {
      self::$_instance = new self();
    }

    return self::$_instance;
  }


  public function getDb()
  {
    if (null === $this->_db)
    {
      $this->_db = new DBManager($this->_dbDetails['host'],
      $this->_dbDetails['username'],
      $this->_dbDetails['password'],
      $this->_dbDetails['database']);

      // If connection could not be established
      if ($this->_db->getErrorCode() != 0)
      {
        // Uncomment to see error messages
        //die('[mysql] Error '.$this->_db->getErrrorCode().': '.
          //$this->_db->getErrorMessage());
        throw new Exception('Unable to connect to database, please check config file or '.
          'manually enable output of mysql error code / message.');
      }
    }

    return $this->_db;
  }

  public function getMap($id, $name = null)
  {
    // Try to add map
    if (!$this->_cache->hasMap($id) && null != $name)
    {
      $this->_cache->add(new Map($id, $name));
    }

    // Map not found?
    if (!$this->_cache->hasMap($id))
    {
      return false;
    }

    return $this->_cache->getMap($id);
  }

  public function getPlayer($id, $login = null, $nick = null)
  {
    if (!$this->_cache->hasPlayer($id) && null != $login)
    {
      $this->_cache->add(new Player($id, $login, $nick));
    }

    // Player not found?
    if (!$this->_cache->hasPlayer($id))
    {
      return false;
    }

    return $this->_cache->getPlayer($id);
  }


  /**
   * Returns whether the DataManager is initialized and associated with
   * a valid database object.
   * @return bool
   */
  public function isInitialized()
  {
    return $this->_initialized && ($this->_db->getErrorCode() == 0);
  }

  public function loadData($request)
  {
    // Check whether DataManager is initialized
    if (!$this->isInitialized())
    {
      throw new Exception("DataManager not initialized.");
    }

    // Create query from request
    $query = new Query($request->toArray());

    // Execute query
    $this->getDb()->executeQuery($query->getSql());

    // Load and return the results
    $data = $this->getDb()->loadResult();

    return $this->_processRecords($data);
  }

  public function setDbDetails($host, $username, $password, $database)
  {
    $this->_dbDetails =
      array('host' => $host, 'username' => $username,
              'password' => $password, 'database' => $database);
    $this->_initialized = true;
  }

}