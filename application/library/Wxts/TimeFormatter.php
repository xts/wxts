<?php
namespace Wxts;

/**
 * Simple time formatter.
 */
class TimeFormatter
{
  public static function asString($time)
  {
    $time = explode('.', $time);

    // Check whether it's more than 60 seconds
    // maximum is 99 seconds, so it can only be 0 or 1 minute
    $minutes = (int) ($time[0] / 60);

    if (!isset($time[1]))
    {
      $time[1] = 0;
    }

    return sprintf('%1$02d:%2$02d.%3$02d', $minutes,
      ($time[0]-60*$minutes),
      $time[1]);
  }
}