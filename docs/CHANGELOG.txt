Changelog:

Git:

- introduced LightVC as a framework for the application
- rewrote most of the application logic
- removed jpgraph dependency and bundled pchart into the application
- added mode display support
- reimplemented graph rendering - should work again now

0.8:

- support for multiple servers added
- global and per server configuration is managed via .ini-files
- garbage-collector for temporary files added
- spline interpolation for time graphs added
- support for time attack and team mode added (please update your plugin file)

0.7:
 
- added player page accessible through simple search form
- new dependency: JpGraph, which is used to render statistical diagrams on
  player pages. Yay!
- front page only shows entries from past 30 days
 
0.6.1:

- bugfix release, the sorting mechanism was broken
- reworked the sorting mechanism to allow deep recursive sorting (for now
  only the fields "Date" and "Map" are supported)
- slightly changed layout

0.6:
- sorting is implemented, although user can't change it yet. Look in the
  method Controller->run() to change the sorting fields (so far only date and
  map are implemented)
- improved performance by removing unneccessary array conversions of
  template data
- added date field in record listing
 
 0.5 (alpha):
 - first release!